import React from 'react';
import { MuiThemeProvider } from '@material-ui/core/styles';
import { withStyles } from '@material-ui/core/styles';
import Theme from './theme';
import Styles from './styles'
import {Paper, Typography, Grid, InputAdornment, TextField, Button} from '@material-ui/core';
import SearchIcon from '@material-ui/icons/Search';
import {getFibonacci} from './services/api'
class App extends React.Component {
  constructor(props) {
    super(props)
    this.state = {
      value: '',
      result : '',
      bigNumber : ''
    }
  }

  onChange = name => event => {
    this.setState({
      [name]: event.target.value
    })
  }


  onCLick = ()=>{
    let {value} = this.state
    let start = performance.now();
    getFibonacci(value,fetch).then(data=>{
      let end = performance.now();
      this.setState({
        result : data.result ? data.result : '',
        bigNumber : data.fibonacci ? data.fibonacci : '',
        time : end-start + ' ms.'
      })
    }).catch(error=>{
      let end = performance.now();
      this.setState({
        result : 'Secuencia demaciado grande',
        bigNumber : '',
        time : end-start + ' ms.'
      })
    })
  }

  render() {
    const { classes } = this.props;
    const { value, result,bigNumber,time } = this.state
    return (
      <MuiThemeProvider theme={Theme}>
        <Grid container spacing={24}>
          <Grid item xs={12}>
            <Paper className={classes.paper}>
              <Typography variant="h5" component="h3">
                Fibonacci App
              </Typography>
              <Typography component="p">
                Set index
              </Typography>
            </Paper>
          </Grid>
          <Grid item xs={12}>
            <Paper className={classes.paper} elevation={0}>
              <Grid container spacing={24}>
                <Grid item xs={12}>
                  <TextField
                    className={classes.margin}
                    id="value"
                    label="Index"
                    value={value}
                    type={'number'}
                    helperText= {time}
                    onChange={this.onChange('value')}
                    InputProps={{
                      startAdornment: (
                        <InputAdornment position="start">
                          <SearchIcon />
                        </InputAdornment>
                      ),
                    }}
                  />
                </Grid>
                <Grid item xs={12}>
                  <Button variant="contained" color="primary" className={classes.button} onClick={this.onCLick}>
                    Search
                  </Button>
                </Grid>
                <Grid item xs={12}>
                  <Typography variant="h6" component="h6">
                    {result}
                  </Typography>
                </Grid>
                <Grid item xs={12}>
                <Typography variant="caption" gutterBottom align="center">
                  {bigNumber}        
                </Typography>
                </Grid>
              </Grid>
            </Paper>
          </Grid>
        </Grid>
      </MuiThemeProvider>
    );
  }

}

export default withStyles(Styles)(App);