const HOST = 'http://localhost:3000'

export const getFibonacci = (value,fetch)=>{
    return new Promise((resolve,reject)=>{
        fetch(HOST+'/fib',{
            method:'POST',
            body : JSON.stringify({
                number : parseInt(value)
            }),
            headers : {
            'Content-Type':'application/json'
            }
        }).then(success=>{
            return success.json()
        }).then(data=>{
           resolve(data)
        }).catch(error=>{
            reject(error)
        })
    })
}

