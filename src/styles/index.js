const styles = theme => ({
  root: {
    flexGrow: 1,
  },
  paper: {
    padding: theme.spacing.unit * 2,
    textAlign: 'center',
    width: '100%',

    color: theme.palette.text.secondary,
  },
  button: {
    margin: theme.spacing.unit,
  },
  });

  export default styles